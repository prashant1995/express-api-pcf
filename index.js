const express = require('express')
const app = express()

const port = process.env.PORT || 3000;

const courses = [
    {id: 1, name: 'Course 1'},
    {id: 2, name: 'Course 2'},
    {id: 3, name: 'Course 3'},
];

app.get('/', function(req, res){
    res.send('Hello It is working!');
});

app.get('/courses', function(req, res){
    res.send(courses);
});

app.listen(port,() => console.log(`Listening to port ${port}...`));